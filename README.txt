
-- SUMMARY --

Allows you to override error messages per field.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- USAGE API --

    1.  Alter the form you want to add custom error messages to
        using hook_form_alter(), or hook_form_ID_alter().
    2.  Add $form['#field_error_message'] = TRUE; to your form
        to apply the custom validation callback.
    3.  Add '#error_message' to your field array, for example:
        $form['body']['#error_message'] = t('custom error');

-- USAGE UI --

    1.  Open the configuration page of the field
        you want to override the error message.
    2.  Enter your override message to the field "Error message".

-- HINTs --

    1.  If you enter a error message to the UI and override it using the API,
        the API wins.
    2.  If the token module is enabled, you can use tokens
        for the error message.


-- CONFIGURATION --

URL = admin/config/user-interface/error-message-override
Click path = Home » Administration » Configuration »
User interface » Error message override

On the backend configuration you can chose how error messages
should be overridden. You could either select to "Override all error messages",
which also will override custom validation message like for
example provided by the email module which will tell the user when they entered
a invalid email address. Or you could chose to
"Override just the 'Required field' message.", which will just override the
message when the user leaves a field empty but it is required.


-- CUSTOMIZATION --

Nope.


-- TROUBLESHOOTING --

Not yet.


-- FAQ --

Nothing yet.


-- CONTACT --

Current maintainers:
* Sebastian Gurlt (sg88) - https://www.drupal.org/user/1592718
