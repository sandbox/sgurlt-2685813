<?php

/**
 * @file
 * Provides the field error message override settings form.
 */

/**
 * Implements hook_form().
 */
function field_error_message_override_settings_form($form) {
  $entity_types = entity_get_info();

  foreach ($entity_types as $entity_name => $entity_type) {

    // Create a fieldset for every entity type.
    $form[$entity_name] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($entity_type['label']),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ($entity_type['bundles'] as $bundle_name => $bundle) {

      // Create a fieldset for every bundle.
      $form[$entity_name][$bundle_name] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($bundle['label']),
        '#collapsible' => FALSE,
      );

      // Create the control field.
      $form[$entity_name][$bundle_name][$entity_name . '_' . $bundle_name . '_error_message_settings'] = array(
        '#type' => 'radios',
        '#title' => t('Error message control'),
        '#description' => t('Select what you want to override.'),
        '#options' => array(
          0 => t('Override all error messages.'),
          1 => t('Override just the "Required field" message."'),
        ),
        '#default_value' => variable_get($entity_name . '_' . $bundle_name . '_error_message_settings', 0),
      );
    }

  }

  return system_settings_form($form);
}
